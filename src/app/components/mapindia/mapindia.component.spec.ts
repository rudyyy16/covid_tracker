import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MapindiaComponent } from './mapindia.component';

describe('MapindiaComponent', () => {
  let component: MapindiaComponent;
  let fixture: ComponentFixture<MapindiaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapindiaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapindiaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
