import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CountryWiseComponent } from './components/country-wise/country-wise.component';
import { WorldWideComponent } from './components/world-wide/world-wide.component';
import { IndiaComponent } from './components/india/india.component';
import { MapindiaComponent } from './components/mapindia/mapindia.component';


const routes: Routes = [
  { path: '', component: WorldWideComponent },
  // { path: '**', component: WorldWideComponent },
  { path: 'world', component: WorldWideComponent },
  { path: 'country', component: CountryWiseComponent },
  // { path: 'map', component: MapindiaComponent},
  { path: 'india', component: IndiaComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
