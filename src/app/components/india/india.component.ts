import { Component, OnInit, ViewChild } from '@angular/core';
import { IndiaService } from 'src/app/services/india.service';
import _ from 'lodash';
import { HostListener } from "@angular/core";
import { ChangeDetectorRef } from '@angular/core';
import {
  ChartComponent,
  ApexAxisChartSeries,
  ApexChart,
  ApexFill,
  ApexYAxis,
  ApexTooltip,
  ApexMarkers,
  ApexXAxis,
  ApexPlotOptions
} from "ng-apexcharts";


export type ChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  xaxis: ApexXAxis;
  yaxis: ApexYAxis | ApexYAxis[];
  labels: string[];
  stroke: any; // ApexStroke;
  markers: ApexMarkers;
  plotOptions: ApexPlotOptions;
  fill: ApexFill;
  tooltip: ApexTooltip;
};

@Component({
  selector: 'app-india',
  templateUrl: './india.component.html',
  styleUrls: ['./india.component.css']
})
export class IndiaComponent implements OnInit {

  dataSource: Object;
  showLoader :boolean = true;
  mapData = [];
  as_of = '';
  data: unknown;
  @HostListener('window:resize', ['$event']) windoww(){

  }
  screenHeight: number;
  screenWidth: number;
  mapwidth = 600;
  mapheight = 600;
  chartwidth = 600;
  chartheight = 400;
  totalObj = {};
  hide = false;
  draggedState = 'india';
  showchart = true;
  // events = {}

  colorrange = {
    "minvalue": "0",
    "startlabel": "Low",
    "endlabel": "High",
    "code": "8ee69a",
    "gradient": "1",
    "color": [{ "maxvalue": "1000", "code": "f7eb00" },
    { "maxvalue": "5000", "code": "d13f17" },
    { "maxvalue": "9000", "code": "ff0505" }]
  };

  @ViewChild("chart") chart: ChartComponent;
  public chartOptions: Partial<ChartOptions>;
  confirmedChart = [];
  recoveredChart = [];
  deathChart = [];
  dateValue = [];
  historyData: any;
  deathRate = '';
  recoveredRate = '';

  constructor(private indiaService: IndiaService,
    private ref: ChangeDetectorRef) {
    this.dataSource = {
      "chart": {
        "animation": "1",
        "showbevel": "0",
        "usehovercolor": "1",
        "showlegend": "1",
        "legendposition": "BOTTOM",
        "legendborderalpha": "0",
        "legendbordercolor": "104fe3",
        "legendallowdrag": "1",
        "legendshadow": "0",
        "caption": "COVID-19 India Confirmed cases",
        "tooltext": "qwe",
        "connectorcolor": "104fe3",
        "fillalpha": "80",
        "hovercolor": "104fe3",
        "theme": "fusion",
        "showMarkerLabels": "1",
        "exposeHoverEvent": "1"
      },
      "colorrange": this.colorrange,
      "data": this.mapData,

    } // end of this.dataSource
  }


  ngOnInit(): void {
    this.setStyle();
    this.chartOptions = {
      series: [
        {
          name: "Confirmed",
          type: "area",
          data: this.confirmedChart,
        },
        {
          name: "Recovered",
          type: "column",
          data: this.recoveredChart,
        },
        {
          name: "Death",
          type: "line",
          data:
            this.deathChart,
        }
      ],
      chart: {
        height: this.chartheight,
        width: this.chartwidth,
        type: "line",
        stacked: false,
        toolbar: {
          show: false
        }
      },
      stroke: {
        width: [0, 2, 5],
        curve: "smooth"
      },
      plotOptions: {
        bar: {
          columnWidth: "50%"
        }
      },

      fill: {
        opacity: [0.85, 0.25, 1],
        gradient: {
          inverseColors: false,
          shade: "light",
          type: "vertical",
          opacityFrom: 0.85,
          opacityTo: 0.55,
          stops: [0, 100, 100, 100]
        }
      },
      labels: this.dateValue,
      markers: {
        size: 0
      },
      xaxis: {
        type: "datetime"
      },
      yaxis: {
        title: {
          text: "Cases"
        },
        min: 0
      },
      tooltip: {
        shared: true,
        intersect: false,
        y: {
          formatter: function (y) {
            if (typeof y !== "undefined") {
              return y.toFixed(0) + " cases";
            }
            return y;
          }
        }
      }
    };
    this.getData();
    // this.indiaService.getStateDistWiseCase().then(res => {
    //   console.log(res);

    // })
  }

  over(e) {
    var lbl = '';
    if (e.dataObj) {
      lbl = e.dataObj.label;
      this.draggedState = e.dataObj.label;
    } else {
      lbl = e;
    }
    if(e === 'india'){
      this.totalObj = this.dataSource['total'];
      this.totalObj['state'] = "India";
    }else{
      this.totalObj = _.find(this.dataSource['data'], { "state": lbl });
    }
    this.deathRate = this.totalObj['confirmed']?(((this.totalObj['deaths']*100)/this.totalObj['confirmed'])+''):'0';
    this.recoveredRate = this.totalObj['confirmed']?(((this.totalObj['recovered'] * 100) / this.totalObj['confirmed']) + ''):'0';
    this.showchart = false;
    this.confirmedChart = [];
    this.recoveredChart = [];
    this.deathChart = [];
    if(e === 'india'){
      this.historyData.forEach((history: any) => {
        this.confirmedChart.push(history['total']['confirmed']);
        this.recoveredChart.push(history['total']['recovered']);
        this.deathChart.push(history['total']['deaths']);
      });
    }else{
      _.forEach(this.historyData, (dataHis) => {
        this.confirmedChart.push(_.find(dataHis['statewise'], { "state": lbl })['confirmed']);
        this.recoveredChart.push(_.find(dataHis['statewise'], { "state": lbl })['recovered']);
        this.deathChart.push(_.find(dataHis['statewise'], { "state": lbl })['deaths']);
      });
    }
    this.chartOptions.series[0].data = this.confirmedChart;
    this.chartOptions.series[1].data = this.recoveredChart;
    this.chartOptions.series[2].data = this.deathChart;

    // console.log(lbl);
    
    // console.log(this.confirmedChart);
    // console.log(this.recoveredChart);
    // console.log(this.deathChart);
    
    
    // this.showchart = true;
    setTimeout(()=>{
      this.showchart =true;
      window.dispatchEvent(new Event('resize'));
    })
    this.ref.detectChanges();
  }

  updateValues(val) {
    console.log(val);

    // this.totalObj = _.find(this.dataSource['data'], { "state": e.dataObj.label })
  }
  out(e) {
    if (this.screenWidth < 500) {
      this.totalObj = this.dataSource['total'];
      this.ref.detectChanges();
    }
  }

  setStyle() {
    this.screenHeight = window.innerHeight;
    this.screenWidth = window.innerWidth;
    // console.log(this.screenWidth, this.screenHeight);
    if (this.screenWidth < 500) {
      this.mapwidth = 380;
      this.mapheight = 500;
      this.chartwidth = 380;
      this.chartheight = 400;
    }

  }

  getData() {
    this.showchart = false;
    this.indiaService.getData().then(res => {
      this.as_of = res['lastRefreshed'];
      this.dataSource['data'] = res['statewise'];
      this.dataSource['total'] = res['total'];
      this.totalObj = this.dataSource['total'];
      this.deathRate = ((this.totalObj['deaths']*100)/this.totalObj['confirmed'])+'';
      this.recoveredRate = ((this.totalObj['recovered']*100)/this.totalObj['confirmed'])+'';
      this.totalObj['state'] = "India";
      this.colorrange['color'][0]['maxvalue'] = (_.maxBy(this.dataSource['data'], "confirmed")['confirmed'] * (1 / 4)) + '';
      this.colorrange['color'][1]['maxvalue'] = (_.maxBy(this.dataSource['data'], "confirmed")['confirmed'] * (3 / 4)) + '';
      this.colorrange['color'][2]['maxvalue'] = _.maxBy(this.dataSource['data'], "confirmed")['confirmed'] + '';


      this.indiaService.getHistoryData().then((res) => {
        this.showchart = true;
        this.historyData = res['data']['history'];
        this.historyData.forEach((history: any) => {
          this.dateValue.push(history['day']);
          this.confirmedChart.push(history['total']['confirmed']);
          this.recoveredChart.push(history['total']['recovered']);
          this.deathChart.push(history['total']['deaths']);
        });
        this.showLoader = false;
      }).catch((err)=>{
        this.showLoader = false;
      });
    }).catch((err)=>{
      this.showLoader = false;
    });
  }


}
